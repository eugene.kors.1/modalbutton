window.onload = function(){ 
    var modal = document.getElementById("modal");
    var btn = document.getElementById("open");
    var trigger = document.getElementsByClassName("close-trigger")[0];
    var accept = document.getElementById("accept");
    var decline = document.getElementById("decline");
    btn.onclick = function() {
    modal.style.display = "flex";
    }
    trigger.onclick = function(event) {
    modal.style.display = "none";
    }
    accept.onclick = function(event) {
    modal.style.display = "none";
    }
    decline.onclick = function(event) {
    modal.style.display = "none";
    }
    window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
    }
};
